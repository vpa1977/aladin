
import cds.aladin.AladinData;
import cds.aladin.AladinException;
import cds.aladin.AladinPlugin;

public class ImageCreationPlug extends AladinPlugin {
   
   public String menu() { return "Image creation with a calibration"; }
   public String description()   {
      return "PLUGIN TUTORIAL:\n" +
             "This plugin is an example of an image plane creation.\n" +
             "It will create an image and will provide an astrometrical " +
             "solution for it.";
   }   
   public String author()        { return "Pierre Fernique [CDS]"; }
   public String version()       { return "1.0 - January 2007"; }
   public String url() { return "http://aladin.u-strasbg.fr/java/Plugins/ImageCreationPlug.java"; }
   public String category()      { return "Plugin tutorial/Image"; }
   
   /*
    * 1) Create a new image in the Aladin stack and gets its corresponding
    *    AladinData object.
    * 2) Generate a pixel array and set them in the new plane
    * 3) Generate a Fits header and set it in the new plan
    */
   public void exec() {
      try {
         AladinData ad = aladin.createAladinData("My Image");
         
         // Pixels
         double [][] pix = new double[512][512];
         for( int x=0; x<512; x++ ) {
            for( int y=0; y<512; y++ ) {
               pix[x][y] = x==255 || y==255 ? 0 : x==0 && y==0 ? 86 : x>300 && y>300 && y%2==0 && x%2==0 ? (x*y)%255 : 255;
            }
         }
         ad.setPixels(pix,16);
         
         // Calibration 
         String header = 
            "SIMPLE  = T\n"+
            "BITPIX  = 16\n"+
            "NAXIS   = 2\n"+
            "NAXIS1  = 512\n"+
            "NAXIS2  = 512\n";
         ad.setFitsHeader(header);
         
      } catch( AladinException e ) { e.printStackTrace(); }
   } 
}