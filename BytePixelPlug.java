

import cds.aladin.*;

public class BytePixelPlug extends AladinPlugin {
   
   public String menu()          { return "Image dump & reverse"; }
   public String description()   {
      return "PLUGIN TUTORIAL:\n" +
             "This plugin is an example for manipulating the byte pixel buffer.\n" +
             "It will create a copy of current plane image, and will reverse" +
             "all pixels.\n\n" +
             "This plugin can be launched by script via the \"inverse\" command.";
   }   
   public String author()        { return "Pierre Fernique [CDS]"; }
   public String version()       { return "1.1 - January 2007"; }
   public String url() { return "http://aladin.u-strasbg.fr/java/Plugins/BytePixelPlug.java"; }
   public String category()      { return "Plugin tutorial/Image"; }
   public String scriptCommand() { return "inverse"; }   
   
   /**
    * 1) Clone image plane via VOAPP 
    * 2) Access to the Aladin byte pixel buffer of this plane
    * 3) Modify these pixels (inverse)
    * 4) Notify Aladin that the byte pixels has been modified => lost the full pixel access
    */
   public void exec() {
      
      try {
         // Dump the plane of the current image
         String labelImg = aladin.getAladinImage().getLabel();
         aladin.execCommand("copy "+labelImg+" copyOf"+labelImg);
         
         // Direct access to the byte pixel buffer of the copied image
         AladinData sd = aladin.getAladinData("copyOf"+labelImg);
         byte pix [] = sd.seeBytePixels();
         
         // Inverse each of them
         for( int i=0; i<pix.length; i++ ) pix[i] = (byte) ~(0xFF & pix[i]);
         
         // Notify Aladin
         sd.bytePixelsModified();
        
      } catch( AladinException e ) {  aladin.warning("Plugin error: "+e.getMessage()); }
   }
}